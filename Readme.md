[![version](https://ad.vgiscience.org/mobile_cart_workshop2020/version.svg)][static-gl-url] [![pipeline](https://ad.vgiscience.org/mobile_cart_workshop2020/pipeline.svg)][static-gl-url]

# Mobile Cartography 2021 Workshop: Social Media, Jupyter Lab & Tag Maps

![Cover Figures](https://ad.vgiscience.org/links/imgs/2019-05-23_emojimap_campus.png)

This is the public repository containing the course material for the
2021 Mobile Cartography Jupyter Lab Workshop.

## Start Jupyter Lab

1. Make sure to connect to TUD network using [a VPN connection][cisco-zih] (Cisco VPN or OpenVPN), otherwise you will not be able to use any of ZIH HPC Services  
2. Add your ZIH-Login to the HPC project, using the pre-shared link
    - this needs to be done only once  
    - it may take up to two hours before your login is available  
3. **Click [this link][spawn-zih-link]** to spawn a Jupyter Lab instance on the TUD ZIH Jupyter Hub.  
    - You'll be requested to login using your TUD Account
        - Do _not_ click "Start My Server".
            - If you see a button or form, click or copy the link again, paste, & the server should start automatically.
        - **Important: Do not change configuration if you see a form:** If the Server doesn't start automatically, click the [ZIH Jupyter link][spawn-zih-link] one more time

A video showing startup and environment selection:

![Startup Jupyter Hub](resources/startup_hub.webm)

Afterwards, once the notebook has opened:  
- If asked to select a Kernel: Confirm suggestion (Python 3) with "Select".  
- execute the first cell, with `SHIFT+ENTER`  
- this will link the conda `workshop_env` to your user folder. Follow any instructions in the notebook.  

## FAQ

Based on the first workshop on January 6, 
we have collected some frequent questions and answers.

Please click on the links below to see possible routes to solve these issues.

1. [**500: Internal Server Error** after clicking spawn the link?](resources/faq/01_error_500.md)
2. [Jupyter Lab Starts, but **the explorer on the left is empty**?](resources/faq/02_explorer_empty.md)
3. [**Cannot activate `workshop_env`**/ the first code cell returns an error?](resources/faq/03_activate_workshop_env.md)
4. [How can I run these notebooks offline (locally)?](resources/faq/04_offline_setup.md)

## HTML Versions of Notebooks

1. [01_raw_intro.html](https://tud_ifk.vgiscience.org/python-datascience-course-2022/01_raw_intro.html)
2. [02_hll_intro.html](https://tud_ifk.vgiscience.org/python-datascience-course-2022/02_hll_intro.html)
3. [03_tagmaps.html](https://tud_ifk.vgiscience.org/python-datascience-course-2022/03_tagmaps.html)
4. [04_topic_classification.html](https://tud_ifk.vgiscience.org/python-datascience-course-2022/04_topic_classification.html)


[spawn-zih-link]: https://taurus.hrsk.tu-dresden.de/jupyter/hub/user-redirect/git-pull?repo=https%3A%2F%2Fgitlab.vgiscience.de%2Ftud_ifk%2Fpython-datascience-course-2022.git&urlpath=lab%2Ftree%2Fpython-datascience-course-2022.git%2Fnotebooks%2F01_raw_intro.ipynb#/~(project~'p_lv_mobicart_2223~runtime~'5*3a00*3a00~cpuspertask~'2~mempercpu~'4000)
[static-gl-url]: https://gitlab.vgiscience.de/tud_ifk/python-datascience-course-2022
[cisco-zih]: https://tu-dresden.de/zih/dienste/service-katalog/arbeitsumgebung/zugang_datennetz/vpn?set_language=de