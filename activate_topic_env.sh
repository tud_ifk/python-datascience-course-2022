#!/bin/sh
# Make central p_lv_mobicart_2021 workerhop_env
# available in user jupyter lab kernel spec
# e.g.
# /home/h5/$USER/.local/share/jupyter/kernels/workshop_env

/projects/p_lv_mobicart_2021/topic_env/bin/python \
    -m ipykernel install \
    --user \
    --name topic_env \
    --display-name="topic_env"