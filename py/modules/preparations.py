"""Workshop preparations"""

import warnings
from matplotlib import MatplotlibDeprecationWarning

def init_packages():
    """Initialize & override deprecation Warnings for the workshop"""
    warnings.filterwarnings(
        "ignore", category=MatplotlibDeprecationWarning)
    warnings.filterwarnings(
        "ignore", category=DeprecationWarning)
    